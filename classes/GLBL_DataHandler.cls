/**
 * @author Xin Wang <xwang4@deloitte.ie>
 * tool for setting up dummy data for project
 *
 * if you need to modify any functions, please, make sure that he functions
 * are tested thoroughly.
 */
public with sharing class GLBL_DataHandler 
	implements 
		Database.Batchable<sObject>,
		Database.AllowsCallouts,
		Database.Stateful{

	// statics members
	public static string namespace = '';
	public static string classname = 'GLBL_DataHandler';

	// non-static members
	public String emailSuffix = '';
	public String action = '';
	public String query = '';
	public Integer batchSize = 2000;
	public List<sObject> objToProcess = new List<sObject>();
	public List<String> objects = new List<String>();
	public Map<String, objectDetail> od = new Map<String, objectDetail>();

	public class GLBL_DataHandlerException extends Exception {}

	public class objectDetail{
		public String objName = '';
		public Integer total = 0;
		public Integer curNum = 0;
	}

	static {
		// @todo
		// naming convention for partial batch recognition will be 
		// implemented for different cases
		namespace = UserInfo.getOrganizationId() + '-' + classname;
	}

	// constructor
	public GLBL_DataHandler() {
		this.emailSuffix = 'novartis.com';
	}

	// constructor override
	public GLBL_DataHandler(GLBL_DataHandlerConfig dhc) {
		this();
		this.init(dhc);
	}

	public void init(GLBL_DataHandlerConfig dhc) {
		if(dhc != null) {
			List<GLBL_DataHandlerConfig.ObjectConfig> tt = dhc.getObjectConfigs();
		}
	}

	public class ObjDetail {
		String name = '';
		String fieldValueType = '';
		String fieldType = '';
		Boolean isNillable = false;
		String label = '';
		String helpText = '';
		Boolean isCustom = false;
		String source = '';
		String objectType = 'Custom';
		String isIndexed = 'Null';

		public ObjDetail(String name , String fieldValueType, Boolean isNillable, String label, String helpText, Boolean isCustom) {
			this.name = name;
			this.fieldValueType = fieldValueType;
			this.isNillable = isNillable;
			this.label = label;
			this.helpText = helpText == null? 'N/A' : helpText;
			this.isCustom = isCustom;
			this.source = 'SFDC';
			this.fieldType = this.isCustom ? 'Custom' : 'Standard';
		}
	}

	@remoteAction
	public static Map<String, list<ObjDetail>> getAllObjects() {
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		Map<String, list<ObjDetail>> t = new Map<String, list<ObjDetail>>();
		Schema.DescribeSObjectResult[] descResult = null;
		for(String k: gd.keyset()) {
			descResult = Schema.describeSObjects(new String[]{k});
			Map<String, Schema.SObjectField> fields = descResult[0].fields.getMap();
			t.put(k, new List<ObjDetail>());
			for(String kk: fields.keyset()) {
				t.get(k).add(new ObjDetail(
					fields.get(kk).getDescribe().getName(), 
					String.valueOF(fields.get(kk).getDescribe().getType()), 
					fields.get(kk).getDescribe().isNillable(),
					fields.get(kk).getDescribe().getLabel(),
					String.valueOf(fields.get(kk).getDescribe().getInlineHelpText()),
					fields.get(kk).getDescribe().isCustom()
				));
			}
		}
		return t;
	}

	/**
	 *  create sobject based a string type
	 * @param {String} typeInStr
	 */
	public static sObject createObjectByTypeInString(String typeInStr) {
		Schema.SObjectType convertType = Schema.getGlobalDescribe().get(typeInStr);
		Sobject obj = convertType.newSObject();
		return obj
	}

	/**
	 * @implement Database.batchable
	 */
   	public Iterable<sObject> start(Database.BatchableContext BC) {
   		if(this.action == 'delete') {
   			this.query = 'Select id from ' + 'currentobjectname limit ' + this.batchSize;
   			List<sObject> obj = Database.query(this.query);
   			return obj;
   		}
   		else if(this.action == 'insert') {
   			List<sObject> obj = this.getSObjectReady(2000);
   		}
   		return this.objToProcess;
   	}

   	public virtual void trackCurrentObjProcessStatus() {
   		// 1. which object am i processing
   		// 2. where am i on this object
   	}

   	/**
   	 * mock creating records
   	 * @param {}
   	 */
   	public virtual List<User> getSObjectReady(Integer num) {
   		if(num > this.batchSize) {
   			throw new GLBL_DataHandlerException('size exceeded you batch allowence.');
   		}
   		List<User> us = new List<User>();
   		for(Integer i = 0; i < num ;i ++) {
   			us.add(new User());
   		}
   		return us;
   	}

   	/**
   	 * @implements batchable
   	 */
   	public void execute(Database.BatchableContext BC, List<sObject> scope){
		this.onExecute(BC, scope, this.action);
	}

	public virtual void onExecute(Database.BatchableContext BC, List<sObject> scope, String action) {
		// this is what you do when execute is in action
	}

	/**
	 * @implements batchable
	 */
   	public void finish(Database.BatchableContext BC){
   		// track current object process status
   		// upadate current query and sobject
   	}

}